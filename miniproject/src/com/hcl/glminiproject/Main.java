package com.hcl.glminiproject;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
	@SuppressWarnings("deprecation")

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Date time=new Date();
		
		List<BillingSystem> bills = new ArrayList<BillingSystem>();
		boolean finalOrder;
		List<Items> items = new ArrayList<Items>();
		Items item1 = new Items(1, "Veg Biryani", 1, 179.0);
		Items item2 = new Items(2, "Curd Rise", 3, 139.0);
		Items item3 = new Items(3, "Egg Biryani", 1, 200.0);
		Items item4 = new Items(4, "Paneer", 2, 256.0);
		Items item5 = new Items(5, "Biryani", 5, 450.0);
		items.add(item1);
		items.add(item2);
		items.add(item3);
		items.add(item4);
		items.add(item5);
		
		System.out.println("WELCOME TO Indian RESTAURANT\n");

		while (true) {
			System.out.println("Please Enter the Login Credentials");

			System.out.println("Email = ");
			String email = scan.next();

			System.out.println("Password = ");
			String password = scan.next();

			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

			System.out.println("Please Enter A if you are Admin and U if you are User ,L to logout");
			String adminOrUser = scan.next();

			BillingSystem bill = new BillingSystem();
			List<Items> selectedItems = new ArrayList<Items>();
			
			double totalCost = 0;
			
			Date date=new Date();
			
			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("E MMM dd  yyyy HH:MM:SS");
			String currentTime = time1.format(dateFormat);

			if (adminOrUser.equals("U") || adminOrUser.equals("u")) {

				System.out.println("Welcome Mr." + name);
				do {
					System.out.println("Today's Menu :- ");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item Code");
					int code = scan.nextInt();
					
					if (code == 1) {
						selectedItems.add(item1);
						totalCost += item1.getItemPrice();
					}

					else if (code == 2) {
						selectedItems.add(item2);
						totalCost += item2.getItemPrice();
					} else if (code == 3) {
						selectedItems.add(item3);
						totalCost += item3.getItemPrice();
					} else if (code == 4) {
						selectedItems.add(item4);
						totalCost += item4.getItemPrice();
					} else {
						selectedItems.add(item5);
						totalCost += item5.getItemPrice();
					}
					
					System.out.println("Press 0 to show bill\nPress 1 to order more");
					int opt = scan.nextInt();
					if (opt == 0)
						finalOrder = false;
					else
						finalOrder = true;

				} while (finalOrder);
				

				System.out.println("Thanks Mr " + name + " for dining in with continental ");
				System.out.println("Items you have Selected");
				selectedItems.stream().forEach(e -> System.out.println(e));
				System.out.println("Your Total bill will be " + totalCost);

				bill.setName(name);
				bill.setCost(totalCost);
				bill.setItems(selectedItems);
				bill.setTime(date);
				bills.add(bill);

			} else if (adminOrUser.equals("A") || adminOrUser.equals("a")) {
				System.out.println("Welcome Admin");
				System.out.println(
						"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
				int option = scan.nextInt();
				switch (option) {
				case 1:
					if ( !bills.isEmpty()) {
						for ( BillingSystem b : bills) {
							if(b.getTime().getDate()==time.getDate()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills today.!");
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (BillingSystem b : bills) {
							if (b.getTime().getMonth()==time.getMonth()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills for this month.!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (BillingSystem b : bills) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total Bill :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
					} else
						System.out.println("No Bills registered yet..!");

					break;

				default:
					System.out.println("Invalid Option");
					System.exit(1);
				}
			} else if (adminOrUser.equals("L") || adminOrUser.equals("l")) {
				System.exit(1);
				
			}else  {
				System.out.println("Invalid Entry");
			}

		}

	}

}